public class Vehiculo {
    private String marca;
    private String matricula;
    private int km;
    private int depostio;

    public Vehiculo (String marca, String matricula,int km,int deposito){
        this.marca=marca;
        this.km=km;
        this.matricula=matricula;
        this.depostio=deposito;
    }

    public void setDepostio(int depostio) {
        this.depostio = depostio;
    }

    public int getDepostio() {
        return depostio;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getKm() {
        return km;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public String toString() {

        String cantidadDeposito;

        if(this.depostio <= 0){
            cantidadDeposito = "No tiene gasolina";
        }else{
            cantidadDeposito = this.depostio+"l";
        }

        return this.marca+" con Matrícula "+this.matricula+"." +
                "\nKilometros: "+this.km+"km." +
                "\nDepósito total: "+cantidadDeposito+".";
    }

    public void llenarDeposito(int litros){
        this.depostio = litros;
    }

    public void avanzarVehiculo(int km){

        if(this instanceof Coche){
            this.depostio = this.depostio - (km*2);
        }else if(this instanceof Moto){
            this.depostio = this.depostio - km;
        }else if(this instanceof Camion) {
            this.depostio = this.depostio - (km * 3);
        }
    }

    public void mostrarInfo(){

        if(this instanceof Coche){
            System.out.println("Coche: "+this.toString());
        }else if(this instanceof Moto){
            System.out.println("Moto: "+this.toString());
        }else if(this instanceof Camion) {
            System.out.println("Camión: "+this.toString());
        }
    }

    public static void main(String[] args) {

        Coche coche = new Coche("Audi", "1234ABC", 174740);
        Moto moto = new Moto("Yamaha", "2233ZBN", 7006);
        Camion camion = new Camion("Pegasso", "5566MNH", 457430);



        coche.avanzarVehiculo(25);
        System.out.println("*************************************");
        moto.avanzarVehiculo(25);
        camion.avanzarVehiculo(25);


        coche.mostrarInfo();
        System.out.println("*************************************");
        System.out.println("*************************************");
        moto.mostrarInfo();
        System.out.println("*************************************");
        System.out.println("*************************************");
        camion.mostrarInfo();
        System.out.println("*************************************");



    }
}



